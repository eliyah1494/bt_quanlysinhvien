// CRUD ~ Creat Read Update Delete
const DSSV = "DSSV";
var dssv = [];

var dataJson = localStorage.getItem(DSSV);

if (dataJson) {
  // truthy falsy
  var dataRaw = JSON.parse(dataJson);
  // var result = [];
  // for (var index = 0; index < dataRaw.length; index++) {
  //   var currentData = dataRaw[index];
  //   var sv = new SinhVien(
  //     currentData.ma,
  //     currentData.ten,
  //     currentData.email,
  //     currentData.matKhau,
  //     currentData.diemToan,
  //     currentData.diemLy,
  //     currentData.diemHoa
  //   );
  //   result.push(sv);
  // }

  dssv = dataRaw.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDssv(dssv);
}
function saveLocalStorage() {
  // lưu  xuống local
  var dssvJson = JSON.stringify(dssv);

  localStorage.setItem(DSSV, dssvJson);
}

function themSv() {
  layThongTinTuForm();

  var newSv = layThongTinTuForm();
  dssv.push(newSv);
  renderDssv(dssv);
  saveLocalStorage();
  resetForm();
}

function xoaSv(idSv) {
  // tìm vị trí
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) {
    return;
  }
  // xoá sinh viên
  dssv.splice(index, 1);
  console.log("dssv: ", dssv.length);

  renderDssv(dssv);

  // update dữ liệu cho localStorage
  // saveLocalStorage();
}

function suaSv(idSv) {
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSv;
  });
  if (index == -1) return;
  var sv = dssv[index];

  // show thông tin lên layout
  showThongTinLenForm(sv);

  // disable input show mã sv
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSv() {
  var svEdit = layThongTinTuForm();
  var index = dssv.findIndex(function (sv) {
    return sv.ma == svEdit.ma;
  });
  if (index == -1) return;
  dssv[index] = svEdit;
  renderDssv(dssv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
}

function timKiemSinhVien() {
  var nameValue = document.getElementById("txtSearch").value;
  var timKiemTen = dssv.filter((value) => {
    return value.ten.toUpperCase().includes(nameValue.toUpperCase());
  });
  console.log("timKiemTen: ", timKiemTen);

  renderDssv(timKiemTen);
}
